<!DOCTYPE html>
<html>
  <?php include("modules/head.html"); ?>
  <body>
    <?php
      include("modules/navbar.html");
    ?>
    <main>
      <div id="main-container">
        <img id="main-img" src="images/resume.jpg" heigth="500px" width="100%" alt="Screen filled with code">
        <div id="main-inner-content">
          <h2 id="main-inner-content-header">Resume</h2>
          <div id="resume-button">
            <p id="main-button-text-invert">Click here to Begin</p>
          </div>
        </div>
      </div>
      <div id="intro">
        <h1 id="intro-header">Want my Simplified Resume?</h1>
      </div>
      <div id="resume-div">
        <p id="resume-title">Resume Document</p>
        <a href="documents/anthony-bren-resume.pdf">
        <div id="resume-view">
          <p>View</p>
        </div>
        </a>
        <a href="documents/anthony-bren-resume.pdf" download>
        <div id="resume-download">
          <img src="images/download-button.png" height="25px" width="25px" alt="Download Button">
        </div>
        </a>
      </div>
      <div id="intro">
        <h1 id="intro-header">Technical Skills</h1>
      </div>

      <div id="card" data-toggle="modal" data-target="#htmlModal">
        <h3>HTML & CSS </h3>
      </div>
      <div id="card" data-toggle="modal" data-target="#jsModal">
        <h3>JavaScript</h3>
      </div>
      <div id="card" data-toggle="modal" data-target="#jqModal">
        <h3>jQuery, jQueryUI, Prototype</h3>
      </div>
      <div id="card" data-toggle="modal" data-target="#bsModal">
        <h3>Bootstrap</h3>
      </div>
      <div id="card" data-toggle="modal" data-target="#phpModal">
        <h3>PHP</h3>
      </div>
      <div id="card" data-toggle="modal" data-target="#sqlModal">
        <h3>SQL</h3>
      </div>
      <div id="card" data-toggle="modal" data-target="#wpModal">
        <h3>Wordpress</h3>
      </div>
      <div id="card" data-toggle="modal" data-target="#jpcModal">
        <h3>Java, Python, C, C#</h3>
      </div>
      <div id="card" data-toggle="modal" data-target="#osModal">
        <h3>Linux and Windows OS Experience</h3>
      </div>

      <p id=separating-line>________________________________________<p>
      <div id="intro">
        <h1 id="intro-header">Social Skills</h1>
      </div>
      <div id="list-item">
        <p id=separating-line>__________<p>
        <h2 id="list-item-header">English and Spanish - Fluent in Both</h2>
        <p id="list-item-content">Having an American dad and a Mexican mom, I was raised speaking both languages. Though I tend to speak english more, I have little trouble talking or translating into spanish.</p>
      </div>
      <div id="list-item">
        <p id=separating-line>__________<p>
        <h2 id="list-item-header">Fast, Adaptable Learner</h2>
        <p id="list-item-content">I tend to pick up on technical skills and well as long-term procedures quickly. Though tooting my own horn is not something I am comfortable doing often, I can guarantee that I will not take long to adapt to the workplace, as has been noted by my mentor as well as my previous employer.</p>
      </div>
      <div id="list-item">
        <p id=separating-line>__________<p>
        <h2 id="list-item-header">Strong Communication Skills</h2>
        <p id="list-item-content">I believe that communication is the most essential part of developing websites as a team. Though I'm not an english scholar, I have solid communication skills, making sure everyone is one the same page, including myself. The form this communication takes varies with the job and the environment, but the importance is all the same. This means I am not afraid to ask many questions in order to get a clear understanding (though I won't get too annoying, promise).</p>
      </div>
      <div id="list-item">
        <p id=separating-line>__________<p>
        <h2 id="list-item-header">Sociable - Experience with Clients</h2>
        <p id="list-item-content">If there was one takeaway from working at McDonald's, it was building my social skills with customers. Though it may seem like a stretch, I was known well for being friendly with customers, making sure they were satisfied. Even if I end up not dealing with clients, I will still do well amongst other co-workers.</p>
      </div>
      <p id=separating-line>________________________________________<p>
      <div id="intro">
        <h1 id="intro-header">Education</h1>
      </div>
      <div id="list-img-item">
        <p id=separating-line>__________<p>
        <img id="img-item" src="images/sibley.jpg">
        <h2 id="img-item-header">Henry Sibley High School</h2>
        <p id="img-item-content">Attended Fall 2014 - Spring 2018. I graduated June 3rd, 2018, and I already have 40 credits of my 60 credit AAS degree in Computer Programming. For explanation on year inconsistencies, please see the note on my Resume document.</p>
      </div>
      <div id="list-img-item">
        <p id=separating-line>__________<p>
        <img id="img-item-long" src="images/inver-hills.png">
        <h2 id="img-item-header">Inver Hills Community College</h2>
        <p id="img-item-content">Attended Fall 2016 - Spring 2017. Here, I completed most of my general credits for my AAS Degree.</p>
      </div>
      <div id="list-img-item">
        <p id=separating-line>__________<p>
        <img id="img-item-long" src="images/saint-paul.jpeg">
        <h2 id="img-item-header">Saint Paul College</h2>
        <p id="img-item-content">Have been attending since Fall 2017. Have recieved a $2,500 Scholarship and am scheduled to complete an AAS degree in Computer Programming with an emphasis on Web Development by June 2019.</p>
      </div>
      <p id=separating-line>________________________________________<p>
      <div id="intro">
        <h1 id="intro-header">Work Experience</h1>
      </div>
      <div id="list-img-item">
        <p id=separating-line>__________<p>
        <img id="img-item-long" src="images/mcdonalds.png">
        <h2 id="img-item-header">McDonald's</h2>
        <p id="img-item-content">Located in Mendota Heights, I worked here from June 2016 to August 2017. Though I don't expect this work experience to impress many future employers, I will leave this here. During the year in which I spent working here, I learned the basics of having a job, including scheduling, financing, and teamwork, as well as customer service. I was well liked and became a crew trainer before resigning to focus on my studies at Saint Paul College.</p>
      </div>
      <a id="option-link" href="contact.php"><p id="contact-whisper">Want to contact me?</p></a>
    </main>

    <?php
      $modalID = "htmlModal";
      $modalTitle = "HTML & CSS";
      $modalBody = "The basic building blocks for building a website, I worked with HTML and CSS a lot when I first started learning web development. " . 
        "Even with a lot being automated now, I still do my HTML and CSS mostly from scratch, especially with this site.";
      $modalExp = "High";
      include("modules/modal.php");

      $modalID = "jsModal";
      $modalTitle = "JavaScript";
      $modalExp = "High";
      include("modules/modal.php");

      $modalID = "jqModal";
      $modalTitle = "jQuery, jQueryUI, Prototype";
      include("modules/modal.php");

      $modalID = "bsModal";
      $modalTitle = "Bootstrap";
      include("modules/modal.php");

      $modalID = "phpModal";
      $modalTitle = "PHP";
      include("modules/modal.php");

      $modalID = "sqlModal";
      $modalTitle = "SQL";
      include("modules/modal.php");

      $modalID = "wpModal";
      $modalTitle = "WordPress";
      include("modules/modal.php");

      $modalID = "jpcModal";
      $modalTitle = "Java, Python, C, C#";
      include("modules/modal.php");

      $modalID = "osModal";
      $modalTitle = "Linux & Windows OS Experience";
      include("modules/modal.php");
    ?>

    <footer>
      <p>&copy; Website and Functions by Anthony Bren, 2018</p>
    </footer>
  </body>
</html>
