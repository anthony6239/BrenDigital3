<div class="modal fade" id=<?php echo $modalID ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $modalTitle ?></h4>
                </div>
                <div class="modal-body">
                    <p><?php echo $modalBody ?></p>
                    <p>Experience Level: <?php echo $modalExp ?></p>
                </div>
            </div>
        </div>
    </div>
</div>