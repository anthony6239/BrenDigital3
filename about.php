<!DOCTYPE html>
<html>
  <?php include("modules/head.html"); ?>
  <body>
    <?php
      include("modules/navbar.html");
    ?>
    <main>
      <div id="main-container">
        <img id="main-img" src="images/about1.jpg" heigth="500px" width="100%" alt="Screen filled with code">
        <div id="main-inner-content">
          <h2 id="main-inner-content-header">About</h2>
          <div id="about-button">
            <p id="main-button-text">Click here to Begin</p>
          </div>
        </div>
      </div>
      <div id="intro">
        <h1 id="intro-header">This Site's Development Process</h1>
      </div>
      <div id="list-item">
        <p id="list-item-content">This is not the first time I have created a personal website. However, this is by far my best one so far. Unlike previous projects, the entire development of this site was documented thoroughly with version control using GitLab, which you can see for yourself below. My main focus on this site, unlike previous sites, was to make it responsive to different screen-widths and platforms, like tablets and mobile. If you want to, feel free to check it out with inspect element, or by looking up this site on your phone. I am not a designer by any means, but I did my best to make this site feel comfortable to view and use from all platforms. The main objective of this site is to demonstrate my current skillset in web development to others. As such, you will see a decent variety of functions and elements, whether efficient or not, in my source code. And while I did take inspiration from different websites, all elements on this site (save for images and icons), as well as internal functions, were made by me from scratch. In the end though, I know that this site is small and superficial, with no back-end. I seek to improve even further on my skillset in the very near future.</p>
      </div>
      <div id="resume-div">
        <a href="https://gitlab.com/anthony6239/BrenDigital3" target="_blank"><div id="link-button"><img id="link-image" src="images/gitlab.png" height="40px"><p id="link-text">View Development Process on GitLab</p></div></a>
      </div>
      <div id="intro">
        <h1 id="intro-header">FAQ</h1>
      </div>
      <div id="list-item">
        <p id=separating-line>__________<p>
        <h2 id="list-item-header">"What's your current career plan?"</h2>
        <p id="list-item-content">Well at the moment, I'm looking for opportunities to begin working as a Front-End Web Developer. As I have one year of school left and I do not have much experience, an internship or part-time job would be both realistic and best for me (Though if you want someone who can work full-time, I can, provided a flexible schedule. I'll be doing very few credits at SPC so I'll have time). I am eager to get to work, so I appreciate any consideration. Once I start getting work experience, I think I'll begin practicing Back-End and App-Level languages, as I want to to be a Full-Stack Developer. Once I finish school, I'll continue my career full-time and increase my level of skill to become a valued member of my team.</p>
      </div>
      <div id="list-item">
        <p id=separating-line>__________<p>
        <h2 id="list-item-header">"I'm noticing a couple of quirks in this site, what gives?"</h2>
        <p id="list-item-content">Most of the quirks have to do with my use of script.aculo.us, a Javascript library that you can find <a href="https://script.aculo.us/">here</a>. I would switch to smoother transitions like the ones CSS provides, but that would defeat the purpose of this website: to show all of my different skills. Working out ways to compensate for janky transitions also gave me a reason to flex my mental muscles on some Javascript and JQuery, which you can find in the head of my source code. I'm not excusing every mistake, as some could be genuinely wrong, but that would explain most of them. Just going through the learning process.</p>
      </div>
      <div id="list-item">
        <p id=separating-line>__________<p>
        <h2 id="list-item-header">"What's with the odd, minimal interface?"</h2>
        <p id="list-item-content">While I aspire to be a Website <i>Developer</i>, I am aware that I am not very good at designing them. I based this simplistic design off of several different, "modern" websites. On top of being cut-and-dry when it comes to design, it is also much cleaner than other projects of mine. Over-all I like the way it turned out, though I <i>am</i> just starting, so hopefully I get better tastes over time. What the client wants will also matter a lot.</p>
      </div>
      <div id="list-item">
        <p id=separating-line>__________<p>
        <h2 id="list-item-header">"Can I give you a suggestion or a criticism?"</h2>
        <p id="list-item-content">Of course! I will be relying on those a lot in my learning process from those with more experience in the field. Feel free to contact me by visiting the icon in the upper-right hand corner, or click down below.</p>
      </div>
      <a id="option-link" href="contact.php"><p id="contact-whisper">Want to contact me?</p></a>
    </main>
    <footer>
      <p>&copy; Website and Functions by Anthony Bren, 2018</p>
    </footer>
  </body>
</html>
