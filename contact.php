<!DOCTYPE html>
<html>
  <?php include("modules/head.html"); ?>
  <body>
    <?php
      include("modules/navbar.html");
    ?>
    <main>
      <div id="main-container">
        <img id="main-img" src="images/contact1.jpg" heigth="500px" width="100%" alt="Screen filled with code">
        <div id="main-inner-content">
          <h2 id="main-inner-content-header">Contact Me!</h2>
          <div id="contact-button">
            <p id="main-button-text">Click here to Begin</p>
          </div>
        </div>
      </div>
      <div id="intro">
        <h1 id="intro-header">The Best Ways to Contact Me</h1>
      </div>
      <div id="list-img-item" class="mobile-contact-item">
        <p id=separating-line>__________<p>
        <img id="img-item" src="images/gmail.png">
        <h2 id="img-item-header">Gmail</h2>
        <p id="img-item-content">Email is by far the most reliable way to contact me. I am usually very fast in responding, and being able to have any information given to me on-hand for later use is a plus. You can contact me at <a href=�mailto:anthony6239@gmail.com�>anthony6239@gmail.com</a></p>
      </div>
      <div id="list-img-item" class="mobile-contact-item">
        <p id=separating-line>__________<p>
        <img id="img-item-medium" src="images/smartphone.png">
        <h2 id="img-item-header">Phone</h2>
        <p id="img-item-content">You can call me anytime, as I always have my phone on me. I seldom miss calls, and it is the fastest way to talk with me. You can reach me by calling 651-747-5250. Additionally, you can call my home phone at 651-455-4161, though cell is preffered.</p>
      </div>
      <div id="list-img-item" class="mobile-contact-item">
        <p id=separating-line>__________<p>
        <img id="img-item-medium" src="images/linkedin-letters.png">
        <h2 id="img-item-header">LinkedIn</h2>
        <p id="img-item-content">If you want to share certain employment information or other things via LinkedIn, feel free! You can reach my LinkedIn profile <a href="https://www.linkedin.com/in/anthony-bren-60048588/">here</a></p>
      </div>
    </main>
    <footer>
      <p>&copy; Website and Functions by Anthony Bren, 2018</p>
      <p>Icons made by <a href="http://www.freepik.com/">Freepik</a> from <a href="http://www.flaticon.com/">www.flaticon.com</p>
    </footer>
  </body>
</html>
