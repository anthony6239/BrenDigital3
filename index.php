<!DOCTYPE html>
<html>
  <?php 
    include("modules/head.html"); 
  ?>
  <body>
    <?php
      include("modules/navbar.html");
    ?>
    <main>
      <div id="main-container">
        <img id="main-img" src="images/home1.jpg" heigth="500px" width="100%" alt="Screen filled with code">
        <div id="main-inner-content">
          <h2 id="main-inner-content-header">Welcome to my Portfolio!</h2>
          <div id="main-button">
            <p id="main-button-text">Click here to Begin</p>
          </div>
        </div>
      </div>
      <div id="intro">
        <h1 id="intro-header">So, what would you like to know?</h1>
      </div>
      <a id="option-link" href="profile.php">
      <div id="profile-option" class="option">
        <h2 id="option-header">Profile</h2>
        <p id="option-content">Learn about me, my story, and my ambitions on my path to become a full-fledged Web Developer.</p>
        <img id="option-image" class="option-image-desktop" src="images/profile1-v.jpg" width="100%" height="550px">
        <img id="option-image" class="option-image-mobile" src="images/profile1-h.jpg" width="100%" height="230px">
      </div>
      </a>
      <a id="option-link" href="resume.php">
      <div id="resume-option" class="option">
        <h2 id="option-header">Resume</h2>
        <p id="option-content">Want to get straight to the point? Take a look at my Resume to see if my skillset could be of use to your team.</p>
        <img id="option-image" class="option-image-desktop" src="images/resume1-v.jpg" width="100%" height="550px">
        <img id="option-image" class="option-image-mobile" src="images/resume1-h.jpg" width="100%" height="230px">
      </div>
      </a>
      <a id="option-link" href="about.php">
      <div id="about-option" class="option">
        <h2 id="option-header">About</h2>
        <p id="option-content-invert">Learn about how this website was made, as well as answers to common questions.</p>
        <img id="option-image" class="option-image-desktop" src="images/about1-v.jpg" width="100%" height="550px">
        <img id="option-image" class="option-image-mobile" src="images/about1-h.jpg" width="100%" height="230px">
      </div>
      </a>
      <a id="option-link" href="contact.php"><p id="contact-whisper">Want to contact me?</p></a>
      </div>
    </main>
    <footer>
      <p><a id="secretPage" href="targetsmasher/targetsmasher.html">&copy;</a> Website and Functions by Anthony Bren, 2018</p>
    </footer>
  </body>
</html>
