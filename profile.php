<!DOCTYPE html>
<html>
  <?php include("modules/head.html"); ?>
  <body>
    <?php
      include("modules/navbar.html");
    ?>
    <main>
      <div id="main-container">
        <img id="main-img" src="images/family.jpg" heigth="500px" width="100%" alt="Screen filled with code">
        <div id="main-inner-content">
          <h2 id="main-inner-content-header">Profile</h2>
          <div id="profile-button">
            <p id="main-button-text">Click here to Begin</p>
          </div>
        </div>
      </div>
      <div id="intro">
        <h1 id="intro-header">A Bit About Myself</h1>
      </div>
      <div id="list-item">
        <p id="list-item-content">I was born on December 1, 1999 in Saint Paul, Minnesota, and have lived in the Twin Cities area ever since. Born to an American dad, who has been a baker all of his life, and a Mexican mom, who clawed her way to becoming a Registered Nurse, I grew up with two different languages and two different cultures. I did well in school, and was able to enter PSEO (Post Secondary Enrollment Options) for 11th and 12th grade. After completing generals for one year, I set my heart on becoming a programmer, encouraged by a mentor of mine. I began learning the basics of Web Development, and started taking classes at a different college just a year later.</p>
      </div>
      <p id=separating-line>__________<p>
      <div id="intro">
        <h1 id="intro-header">My Start with Coding</h1>
      </div>
      <div id="list-item">
        <p id="list-item-content">If I told you my relative skill with computers initially came from a love for coding, I would be lying. The reality of it is that video-games are what initially drew me to computers. A steady stream of video-gaming led me to have a good deal more knowledge about computers then either of my parents. While the idea of making a living off of programming had crossed my mind, I had never truly considered it as a viable option. It was in my early teens when someone, who would go on to be a teacher and mentor of mine, began to show me how to code. I proved a good learner, and a relatively skilled coder. With practice, I got a solid foothold in the ways of website development. Now with that as my major, this website is for all to see my progress.</p>
      </div>
      <p id=separating-line>__________<p>
      <div id="intro">
        <h1 id="intro-header">My Ambitions for the Future</h1>
      </div>
      <div id="list-item">
        <p id="list-item-content">After starting to take programming, I have found out that the world of programming, even when narrowed down to web development, is massive. Though I have many front-end essentials down, I am well aware that I am far from a full-fledged developer. I will need to adjust myself to a working climate I have not experienced terribly often, and add even more to my skill set. However, I am motivated to learn and grow as a developer, and my work ethic follows that motivation. I aspire to be a Full-stack developer, and I will work my way to it one way or another.</p>
      </div>
      <p id=separating-line>__________<p>
      <div id="intro">
        <h1 id="intro-header">What I bring to your Workplace</h1>
      </div>
      <div id="list-item">
        <p id="list-item-content">Though I cannot bring much experience right now, what I can bring is my eagerness to learn, improve, and master my craft. I am a pretty fast learner, and will do my absolute best to contribute significantly to the team and our projects, even if it means feeling overwhelmed at times. I do well in following the instructions of my seniors and peers. In essence, I can offer you only my word that I work hard and earnestly to make a positive difference in my work environment.</p>
      </div>
      <a id="option-link" href="contact.php"><p id="contact-whisper">Want to contact me?</p></a>
    </main>
    <footer>
      <p>&copy; Website and Functions by Anthony Bren, 2018</p>
    </footer>
  </body>
</html>
